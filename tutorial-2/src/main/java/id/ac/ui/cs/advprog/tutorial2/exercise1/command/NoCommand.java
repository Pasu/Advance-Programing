package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

public class NoCommand implements Command {

    @Override
    public void execute() {
        // Do nothing
        System.out.println("No Command");
    }

    @Override
    public void undo() {
        // Do nothing
    }
}
