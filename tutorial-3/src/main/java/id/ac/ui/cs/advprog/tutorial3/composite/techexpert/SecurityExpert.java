/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

/**
 *
 * @author Pasu Siahaan
 */

public class SecurityExpert extends Employees{

    public SecurityExpert(String name,double salary) {
        if(salary<70000.00){
            throw new IllegalArgumentException();
        }
        this.name = name;
        this.salary = salary;
        this.role = "Security Expert";
    }
    @Override
   public double getSalary() {
        //TODO Implement
        return this.salary;
    }
   @Override
    public String getName() {
        return this.name;
    }
    @Override
    public String getRole(){
        return this.role;
    }
}
