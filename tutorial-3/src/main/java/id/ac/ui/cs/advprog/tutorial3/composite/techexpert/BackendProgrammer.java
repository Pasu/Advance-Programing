package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class BackendProgrammer extends Employees {
    //TODO Implement
    
    public BackendProgrammer(String name, double salary) {
        if(salary<20000.00){
            throw new IllegalArgumentException();
        }
        this.name = name;
        this.salary = salary;
        this.role = "Back End Programmer";
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return this.salary;
    }

   
    @Override
    public String getName() {
        return this.name;
    }
    @Override
    public String getRole(){
        return this.role;
    }
}
