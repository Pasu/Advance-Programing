/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

/**
 *
 * @author Pasu Siahaan
 */

public class ThickBunBurger extends Food {
    public ThickBunBurger() {
        //TODO Implement
        this.description = "Thick Bun Burger";
    }

    @Override
    public double cost() {
        //TODO Implement
        return 2.50;
    }
}