/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

/**
 *
 * @author Pasu Siahaan
 */

public class ChiliSauce extends Food {
    Food food;

    public ChiliSauce(Food food) {
        //TODO Implement
        this.food = food;
    }

    @Override
    public String getDescription() {
        //TODO Implement
        return food.getDescription()+", adding chili sauce";
    }

    @Override
    public double cost() {
        //TODO Implement
        return food.cost()+0.3;
    }
}