import org.junit.Test;
import org.junit.Before;

import static org.junit.Assert.*;

public class RentalTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    @Test
    public void getMovie() {
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        Rental rent = new Rental(movie, 3);

        assertEquals(movie, rent.getMovie());
    }

    @Test
    public void getDaysRented() {
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        Rental rent = new Rental(movie, 3);

        assertEquals(3, rent.getDaysRented());
    }
    @Test
    public void getAmount(){
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        Rental rent = new Rental(movie, 3);
        assertEquals(3.5,rent.getAmount(),0.0);
        movie = new Movie("Who Killed Captain Alex?", Movie.CHILDREN);
        rent = new Rental(movie, 4);
        assertEquals(3.0,rent.getAmount(),0.0);
        movie = new Movie("Who Killed Captain Alex?", Movie.NEW_RELEASE);
        rent = new Rental(movie, 3);
        assertEquals(9.0,rent.getAmount(),0.0);
    }

    @Test
    public void getFrequentRenterPoint(){
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.NEW_RELEASE);
        Rental rent = new Rental(movie, 3);
        assertEquals(2,rent.getFrequentRenterPoints());
    }

}