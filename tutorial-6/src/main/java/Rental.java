class Rental {

    private Movie movie;
    private int daysRented;

    public Rental(Movie movie, int daysRented) {
        this.movie = movie;
        this.daysRented = daysRented;
    }

    public Movie getMovie() {
        return movie;
    }

    public int getDaysRented() {
        return daysRented;
    }
    public double getAmount(){
        double thisAmount = 0;
        if (this.movie.getPriceCode()==Movie.REGULAR) {
            thisAmount += 2;
            if (getDaysRented() > 2){
                thisAmount += (getDaysRented() - 2) * 1.5;
            }
        }
        else if(this.movie.getPriceCode()==Movie.NEW_RELEASE){
            thisAmount += getDaysRented() * 3;
        }
        else if(this.movie.getPriceCode()==Movie.CHILDREN){
            thisAmount += 1.5;
            if (getDaysRented() > 3){
                thisAmount += (getDaysRented() - 3) * 1.5;
            }
        }
        
        return thisAmount;
    }

    public int getFrequentRenterPoints(){
        int frequentRenterPoints = 1;
        if ((this.movie.getPriceCode() == Movie.NEW_RELEASE) &&
            this.daysRented > 1)
            frequentRenterPoints++;
        return frequentRenterPoints;
    }

}