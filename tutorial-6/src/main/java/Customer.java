import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public String statement() {
        String result = "Rental Record for " + getName() + "\n";
        Iterator<Rental> iterator = rentals.iterator();
        while (iterator.hasNext()) {
            Rental each = iterator.next();
            double thisAmount = each.getAmount();

            // Determine amount for each line

            // Add frequent renter points


            // Add bonus for a two day new release rental


            // Show figures for this rental
            result += "\t" + each.getMovie().getTitle() + "\t" +
                    String.valueOf(thisAmount) + "\n";
        }

        // Add footer lines
        result += "Amount owed is " + String.valueOf(totalAmount()) + "\n";
        result += "You earned " + String.valueOf(frequentRenterPoints()) + " frequent renter points";

        return result;

    }
    public int frequentRenterPoints(){
        int point=0;
        Iterator<Rental> i = rentals.iterator();
        while(i.hasNext()) {
            Rental j = i.next();
            point += j.getFrequentRenterPoints();
        }
        return point;
    }
    public double totalAmount(){
        double amounts = 0;
        Iterator<Rental> i = rentals.iterator();
        while(i.hasNext()){
            Rental j = i.next();
            amounts += j.getAmount();
        }
        return amounts;
    }

}