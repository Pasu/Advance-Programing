/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

/**
 *
 * @author Pasu Siahaan
 */
public class RedCheese implements Cheese{
    @Override
    public String toString() {
        return "Red Cheese";
    }
}
