/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

/**
 *
 * @author Pasu Siahaan
 */
public class RedVeggies implements Veggies{
    public String toString() {
        return "Red Veggies";
    }
}
