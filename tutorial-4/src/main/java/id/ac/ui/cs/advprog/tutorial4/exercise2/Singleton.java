package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {

    //TODO Implement me!
    private static Singleton i;
    //What's missing in this Singleton declaration?

    //

    //
    private Singleton() {

    }

    public static Singleton getInstance() {
        // TODO Implement me!
        if(i== null) {
            i = new Singleton();
        }
        return i;
    }
}
